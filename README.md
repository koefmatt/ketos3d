# Ketos3D 3D Printer Project
Ketos3D is a reverse engineering effort of the Cetus3D printer. Its goal is
not to replicate or counterfit the superb Cetus3D printer but rather to
enhance it and unlock its full potential.

## Main Goals
  - Make It Silent: Using Trinamic stepper motor drivers instead of the
    current DRV8824 drivers from Ti will make it practically inaudible.
  - Make It Interoperable: Replace the closed-source CPU module with a Marlin
    compatible or another board (there already are boards like that out there,
    but they're not open source either).
